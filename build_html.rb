#!/usr/bin/env ruby

require 'fileutils'

unless system('which grip')
  unless system('pip install grip')
    raise "Installing grip failed"
  end
end

Dir.chdir(__dir__) do
  system "grip TechnicalGuideWrappedMultisig.md --title='A Technical Guide to The Wrapped Multisig Contract' --export"
  FileUtils.mv 'TechnicalGuideWrappedMultisig.html', 'public/index.html'
end

