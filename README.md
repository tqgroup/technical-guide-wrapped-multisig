
# A Technical Guide to the Wrapped Multisig Contract

The entire site is based out of the
[`TechnicalGuideWrappedMultisig.md`](TechnicalGuideWrappedMultisig.md) file.

To build the HTML from the markdown file, either run `build_html.rb` or simply
commit anything (a pre-commit hook will rebuild the HTML).

## Debugging

See the [plain html](https://gitlab.com/pages/plain-html) GitLab Pages
template for more information.

