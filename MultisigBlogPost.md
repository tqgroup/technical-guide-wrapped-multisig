# Introduction

Imagine you're buying a car through a dealership's smart contract on the Tezos blockchain.

You've selected the make and model, but before you can complete the purchase,
you're going to need:
  - a valid driver's license
  - proof of insurance
  - approval from the loan issuer 
  - approval from the dealership

Today the dealer processes all of these steps. Tomorrow a
smart contract will collect these documents and unlock your car.

To ensure that all of the necessary approvals are in place before releasing an asset, our contract can use a method called _multisig_.


## Summary

In this post, I'll be explaining:
- What is a multisig contract?
- How does it work?
- Why does it matter?
- Why use the Multisig Wrapper instead of the Generic Multisig contract?

TL;DR: Multisig contracts allow quorums of signers to sign operations.
Generic Multisig is like a user who does what's signed by a quorum.
The Multisig Wrapper makes a contract require a quorum.


# What is a multisig contract?

Multisignature (multisig) is a digitial signature scheme that allows
multiple signers to cooperatively sign data.
Properly implemented digital signatures offer authentication, integrity,
and non-repudiation.

Here, "signing" some data with a (secret) key means somehow combining the data and key in such a
way that:
  1. Anyone can check that the signature matches the data
  2. Anyone can verify whose (public) key cooresponds to the signature

These signers can be individuals, institutions, scripts, or smart contracts.

How cooperatively?
Most multisig schemes require `n` out of `m` signers to be present for
their combined signature to be valid.
For example, if `n = 6` and `m = 10` then at least `60%` of signers must agree.

In our case, what's being signed is a specific operation or action that we want to execute on the blockchain (in our case, Tezos).

Thus a _"multisig contract"_ is a smart contract that requires cooperation of
some signers to execute operations.


# How do multisig contracts work?

What does it look like to use a multisig contract?

The steps to setting up and interacting with a multisig contract are:
1. Originate a new copy of the contract, specifying the list of signers and quorum requirements
2. Create an object that represents the action you wish to perform
3. A quorum of signers use their secret keys to independently sign the value
4. The signatures are collected and sent, along with the signed value, to the contract
5. The contract validates the signatures and, if a quorum is present, it executes the corresponding action


# Why do multisig contracts matter?

Here are a few reasons:

- To prevent having a single point of failure: in a "unisig" contract, a single private key is solely responsible for all administration.
- To allow a group, e.g. a board of directors or operations team, to jointly administer a contractx

# Comparison of the contracts

The [Generic Multisig contract](https://github.com/murbard/smart-contracts/blob/master/multisig/michelson/generic.tz)
accepts an arbitrary code block as its parameter. If a quorum of signers sign
the code block, it is executed by the contract.
To receive and process this execution, a contract must recognize your Generic Multisig contract as an authorized user.

The [Multisig Wrapper contract](https://assets.tqtezos.com/contracts/multisig-wrapper)
is specialized to a particular contract, which it wraps.
It only accepts the parameters of the wrapped contract, along with a list of signatures and some metadata.


# Why use the Multisig Wrapper instead of the Generic Multisig contract?

Analogies:
- The Generic Multisig contract is like a blockchain user whose action is voted upon.
- The Multisig Wrapper contract is like a multi-key padlock that can be added to an existing door (contract).


## Practical limits of Arbitrary code

Consider the following three code blocks, which could be passed to the
Generic Multisig contract:

```haskell
PUSH mutez 10; PUSH address SOME_ADDRESS; TRANSFER_TOKENS;
DIP { NIL operation; }; CONS;

PUSH mutez 10; PUSH address SOME_ADDRESS; TRANSFER_TOKENS; DUP;
DIP { DIP { NIL operation; }; CONS; }; CONS;

BALANCE; PUSH address SOME_ADDRESS; TRANSFER_TOKENS;
DIP { NIL operation; }; CONS;
```

Can you guess what each does?

- The first one transfers `10 μꜩ` to `SOME_ADDRESS`
- The second one transfers `20 μꜩ` to `SOME_ADDRESS`
- The third one transfers all of your `ꜩ` to `SOME_ADDRESS`

In short, when the multisig governs arbitrary code, it can be difficult to tell
what the Michelson code is doing, especially in the absence of a formal proof.


## Pre-approval

The Generic Multisig is not specific to any particular contracts, parameters,
or addresses, so actions can be modified so that they:
- Send `ꜩ` to a different address
- Send the parameters to a different contract
- Modify the parameters before sending them to a contract

How can a user interface make this friendly to work with?

One solution, used in the
[Tezos Client](tezos.gitlab.io/master/api/cli-commands.html)'s
commands to interact with the Generic Multisig, is to have each signer
independently sign a built-in operation, e.g. transfer or delegate.
These built-in operations are easy to read, but new operations
have to be added manually.

With the Multisig Wrapper, you can only sign a quorum/signer list update
or exactly the parameters of the specific contract that it wraps.

This allows you to use any existing methods to interact with the base contract
and rely on the multisig wrapped version to only accept quorum-signed
parameters that are inputs to the base contract.


# Conclusion

While the Generic Multisig contract is very flexible and allows a variety of
different operations to be signed and executed, its support for arbitrary
Michelson code may make it _too flexible_ for your application.

The Multisig Wrapper contract provides all of the security of the
Generic Multisig, specified to your particular use case.

